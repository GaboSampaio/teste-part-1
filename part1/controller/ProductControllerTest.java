package com.gabo.part1.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import com.gabo.part1.model.Product;
import com.gabo.part1.repository.ProductRepository;
import com.gabo.part1.service.ProductService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Transactional
public class ProductControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;
    @MockBean
    private ProductRepository productRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ProductController productController;
    @Autowired
    ProductService productService;

    @Test
    void testAddOnceShouldReturn201() {

        Product product = new Product(1, "TestSameName", LocalDateTime.now());
        productController.add(product);
        Assertions.assertTrue(productController.add(product).getStatusCode().is2xxSuccessful());
    }

    @Test
    void testAddTwiceRecentShouldReturn403() {
        Product product = new Product(1, "TestName", LocalDateTime.now());
        Product product2 = new Product(1, "TestName", LocalDateTime.now());
        productController.add(product);
        productService.add(product);
        System.out.println(productService.findAll().size()+"=========-=-=-=-=-=");
        Optional<Product> tmpProduct = productService.findById(1);
        ResponseEntity<String> response = restTemplate.postForEntity("/v1/products", product,String.class);
        productController.add(product2);
        ResponseEntity<String> response2 = restTemplate.postForEntity("/v1/products", product2,String.class);
        Assertions.assertTrue(response2.getStatusCode().is2xxSuccessful());
    }
}